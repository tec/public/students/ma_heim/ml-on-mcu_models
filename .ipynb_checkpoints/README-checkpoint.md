# Machine Learning on Microcontrollers - Neural Network Models

Part of the thesis [Evaluation and Deployment of Resource-Constrained Machine Learning on Embedded Devices](https://gitlab.ethz.ch/tec/public/students/ma_heim).
This repository contains the trained models which were evaluated and deployed on microcontrollers:

- LeNet5 on MNIST
- ResNet-20 on CIFAR-10

The models were trained and saved as a Keras file by using TensorFlow.

For the conversion process to `.tflite` see the [toolchain repository](../ML-on-MCU_toolchain).